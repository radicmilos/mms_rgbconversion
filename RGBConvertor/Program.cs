﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;

namespace RGBConvertor
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap bitmap = new Bitmap(args[0]);

            bitmap.Save("output_jpg.jpg", ImageFormat.Jpeg);
            bitmap.Save("output_png.png", ImageFormat.Png);
            bitmap.Save("output_bmp.bmp", ImageFormat.Bmp);

            ConvertToGrayscale(false, bitmap, "output_grayscale_lightness.tif");
            ConvertToGrayscale(true, bitmap, "output_grayscale_average.tif");
        }

        static int LightnessMethodGrayscale(int r, int g, int b)
        {
            int maxrgb = System.Math.Max(System.Math.Max(r, g), b);
            int minrgb = System.Math.Min(System.Math.Min(r, g), b);

            return (maxrgb + minrgb) / 2;
        }

        static int AverageMethodGrayscale(int r, int g, int b)
        {
            return (r + g + b) / 3;
        }

        static void ConvertToGrayscale(bool useAverage, Bitmap bmp, string path)
        {
            int width = bmp.Width;
            int height = bmp.Height;

            Bitmap outImage = new Bitmap(bmp);

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    var p = bmp.GetPixel(x, y);

                    int alpha = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    int avg;
                    if (useAverage)
                    {
                        avg = AverageMethodGrayscale(r, g, b);
                    }
                    else
                    {
                        avg = LightnessMethodGrayscale(r, g, b);
                    }

                    outImage.SetPixel(x, y, Color.FromArgb(alpha, avg, avg, avg));
                }
            }
            outImage.Save(path, ImageFormat.Bmp);
        }
    }
}
